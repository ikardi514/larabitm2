<header class="intro-header" style="background-image: url('cleanblog/img/{{$background}}')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>{{$sectionTitle}}</h1>
                    <hr class="small">
                    <span class="subheading">{{$subtitle}}</span>
                </div>
            </div>
        </div>
    </div>
</header>